module.exports = {
    tabWidth: 4,
    eslintIntegration: true,
    singleQuote: false,
    bracketSpacing: true,
    printWidth: 150,
    trailingComma: "es5",
};
